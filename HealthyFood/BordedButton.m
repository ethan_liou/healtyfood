//
//  BordedButton.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/31.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "BordedButton.h"

@interface BordedButton()

@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat roundedRadius;
@property (nonatomic, strong) IBInspectable UIColor * borderColor;


@end

@implementation BordedButton

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    self.layer.borderColor = _borderColor.CGColor;
    self.layer.borderWidth = _borderWidth;
    self.layer.cornerRadius = _roundedRadius;
}

@end
