//
//  MeasureTableViewCell.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/28.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "MeasureTableViewCell.h"
#import "QuartzDrawView.h"

@interface MeasureTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel * name;
@property (nonatomic, weak) IBOutlet UILabel * bpm;
@property (nonatomic, weak) IBOutlet UILabel * timeLbl;
@property (nonatomic, weak) IBOutlet QuartzDrawView * drawV;

@end

@implementation MeasureTableViewCell

-(void)setMeasure:(Measure*)measure
{
    _measure = measure;
    _name.text = _measure.user.friendName;
    _bpm.text = [NSString stringWithFormat:@"%d",_measure.bps];
    _drawV.lineColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.1];
    [_drawV setBuffer:_measure.rawData];
}

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
