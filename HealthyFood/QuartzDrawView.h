//
//  QuartzDrawView.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/22.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuartzDrawView : UIView

@property (nonatomic, strong) UIColor * lineColor;

-(NSData*)getBuffer;
-(void)setBuffer:(NSData*)d;
-(void)insertData:(float)data;
-(CGFloat)isMinPeak;
-(CGFloat)isMaxPeak;
@end
