//
//  Measure.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/22.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Friend.h"
#import "Location.h"

@interface Measure : NSObject<NSCopying>

@property (nonatomic) int objId;
@property (nonatomic) int timing;
@property (nonatomic, strong) NSDate * measureTime;
@property (nonatomic) int bps;
@property (nonatomic, strong) Friend * user;
@property (nonatomic, strong) NSData * rawData;
@property (nonatomic, strong) Location * location;
@property (nonatomic) int reference;

+(Measure*)getMeasureById:(int)objId;
+(Measure*)getLastMeasure;
+(NSMutableArray*)getTodayMeasure;
-(void)save;
+(NSArray*)getMeasuresInLocID:(long long)locId;

@end
