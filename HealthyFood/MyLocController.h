//
//  MyLocController.h
//  HealthyFood
//
//  Created by Ethan on 2015/4/6.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLLocation;
@interface MyLocController : NSObject

typedef void (^LocationCallback)(CLLocation * loc);
typedef void (^LocationErrCallback)(NSString * errStr);

+(MyLocController*)sharedInstance;
-(void)getLocationWithSuc:(LocationCallback)callback WithErr:(LocationErrCallback)errCallback;

@end
