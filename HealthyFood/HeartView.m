//
//  HeartView.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/17.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "HeartView.h"

@implementation HeartView

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        _lineColor = [UIColor redColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    float midX = CGRectGetMidX(rect);
    
    CGPoint startPoint = {midX, rect.size.height * 0.3};
    CGPoint endPoint = {midX, rect.size.height * 0.7};
    
    // First control point (top)
    CGPoint cp1 = {rect.size.width / 2 * 0.45, rect.size.height * 0.1};
    // Second control point (bottom)
    CGPoint cp2 = {rect.size.width / 2 * 0.95, rect.size.height * 0.43};
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:self.bounds];

    UIBezierPath *transparentPath = [UIBezierPath bezierPath];
    [transparentPath moveToPoint:CGPointMake(startPoint.x, startPoint.y)];
    [transparentPath addCurveToPoint:CGPointMake(endPoint.x, endPoint.y) controlPoint1:CGPointMake(midX+cp1.x, cp1.y) controlPoint2:CGPointMake(midX+cp2.x, cp2.y)];
    [transparentPath addCurveToPoint:CGPointMake(startPoint.x, startPoint.y) controlPoint1:CGPointMake(midX-cp2.x, cp2.y) controlPoint2:CGPointMake(midX-cp1.x, cp1.y)];
    [transparentPath closePath];
    [[UIColor redColor] setStroke];
    transparentPath.lineWidth = 4.0f;
    [transparentPath stroke];

    [overlayPath appendPath:transparentPath];
    [overlayPath setUsesEvenOddFillRule:YES];
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = overlayPath.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor whiteColor].CGColor;
    
    [self.layer addSublayer:fillLayer];
}

@end
