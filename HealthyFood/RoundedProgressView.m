//
//  RoundedProgressView.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/16.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "RoundedProgressView.h"

@interface RoundedProgressView()

@property (nonatomic, strong) CAShapeLayer * shapeLayer;

@end

@implementation RoundedProgressView

-(void)setProgress:(CGFloat) progress{
    _progress = progress;
    dispatch_async(dispatch_get_main_queue(), ^{
        _shapeLayer.strokeEnd = _progress;
    });
}

-(void)incProgress:(CGFloat) progress{
    __block float fromProgress = _progress;
    _progress += progress;
    __block float toProgress = _progress;
    dispatch_async(dispatch_get_main_queue(), ^{
        [_shapeLayer removeAllAnimations];
        CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        drawAnimation.duration            = 0.15;
        drawAnimation.fromValue = [NSNumber numberWithFloat:fromProgress];
        drawAnimation.toValue   = [NSNumber numberWithFloat:toProgress];
        drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        [_shapeLayer addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
        _shapeLayer.strokeEnd = toProgress;
    });
}

- (id)initWithCoder:(NSCoder*)coder{
    self = [super initWithCoder:coder];
    if(self){
        _color = [UIColor colorWithRed:0xAA/255.0f green:0xDF/255.0f blue:0xF1/255.0f alpha:1.0f];
        _progress = 0.0f;
        _width = 30.0f;
        _startAngle = -M_PI_2;
        _endAngle = -M_PI_2 - 2 * M_PI;
        _clockwise = NO;
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self addLayer:self.bounds];
}

- (void)drawRect:(CGRect)rect{
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(currentContext, 1.0);
    CGContextSetStrokeColorWithColor(currentContext, [UIColor lightGrayColor].CGColor);
    CGContextBeginPath(currentContext);
    CGContextAddPath(currentContext, [UIBezierPath bezierPathWithArcCenter:CGPointMake(rect.size.width/2, rect.size.height/2) radius:rect.size.width / 2 - 0.5 startAngle:0 endAngle:M_PI * 2 clockwise:_clockwise].CGPath);
    CGContextDrawPath(currentContext, kCGPathStroke);
    CGContextAddPath(currentContext, [UIBezierPath bezierPathWithArcCenter:CGPointMake(rect.size.width/2, rect.size.height/2) radius:rect.size.width / 2 - 1 - _width - 0.5 startAngle:0 endAngle:M_PI * 2 clockwise:_clockwise].CGPath);
    CGContextDrawPath(currentContext, kCGPathStroke);
}

- (void)addLayer:(CGRect)rect {
    CAShapeLayer *progressLayer = [CAShapeLayer layer];
    progressLayer.frame = rect;
    progressLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(rect.size.width/2, rect.size.height/2) radius:rect.size.width / 2 - _width / 2 - 1 startAngle:_startAngle endAngle:_endAngle clockwise:_clockwise].CGPath;
    progressLayer.lineWidth = _width;
    progressLayer.strokeColor = _color.CGColor;
    progressLayer.fillColor = [UIColor clearColor].CGColor;

    [self.layer addSublayer:progressLayer];
    _shapeLayer = progressLayer;
    _shapeLayer.strokeEnd = 0.0f;
    
//    
//    
//    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
//    gradientLayer.frame = rect;
//    gradientLayer.locations = 
//    gradientLayer.colors = [NSArray arrayWithObjects:[UIColor whiteColor].CGColor, _color.CGColor, nil];
//    
//    [gradientLayer setMask:progressLayer];
//    [self.layer addSublayer:gradientLayer];

}

@end
