//
//  ResultDrawView.m
//  HealthyFood
//
//  Created by Ethan on 2015/4/6.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "ResultDrawView.h"

@interface ResultDrawView()

@property (nonatomic, strong) CAShapeLayer * shapeLayer;
@property (nonatomic) CGFloat startAngle;
@property (nonatomic) CGFloat endAngle;
@property (nonatomic) BOOL clockwise;
@property (nonatomic) CGFloat width;

@end

@implementation ResultDrawView

-(void)setProgress:(CGFloat) progress{
    _progress = progress;
    dispatch_async(dispatch_get_main_queue(), ^{
        _shapeLayer.strokeEnd = _progress;
    });
}

- (id)initWithCoder:(NSCoder*)coder{
    self = [super initWithCoder:coder];
    if(self){
        _color = [UIColor colorWithRed:0xAA/255.0f green:0xDF/255.0f blue:0xF1/255.0f alpha:1.0f];
        _progress = 0.0f;
        _startAngle = -3 * M_PI / 2 + M_PI / 6;
        _endAngle = M_PI / 3;
        _clockwise = YES;
        _width = 30.0f;
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self addLayer:self.bounds];
}

- (void)drawRect:(CGRect)rect{
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(currentContext, _width);
    CGContextSetStrokeColorWithColor(currentContext, [UIColor whiteColor].CGColor);
    CGContextBeginPath(currentContext);
    UIBezierPath * path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(rect.size.width/2, rect.size.height/2) radius:rect.size.width / 2 - _width / 2 - 1 startAngle:_startAngle endAngle:_endAngle clockwise:_clockwise];
    CGContextAddPath(currentContext, path.CGPath);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat components[4] = {0.0, 0.0, 0.0, 0.5};
    CGColorRef shadowColor = CGColorCreate(colorSpace, components);
    CGContextSetShadowWithColor(currentContext, CGSizeMake(0.0f,3.0f), 5.0, shadowColor);
    CGContextDrawPath(currentContext, kCGPathStroke);
}

- (void)addLayer:(CGRect)rect {
    CAShapeLayer *progressLayer = [CAShapeLayer layer];
    progressLayer.frame = rect;
    progressLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(rect.size.width/2, rect.size.height/2) radius:rect.size.width / 2 - _width / 2 - 1 startAngle:_startAngle endAngle:_endAngle clockwise:_clockwise].CGPath;
    progressLayer.lineWidth = _width;
    progressLayer.strokeColor = _color.CGColor;
    progressLayer.fillColor = [UIColor clearColor].CGColor;
    
    [self.layer addSublayer:progressLayer];
    _shapeLayer = progressLayer;
    _shapeLayer.strokeEnd = 0.0f;
}

@end
