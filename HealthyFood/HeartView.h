//
//  HeartView.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/17.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>

IBInspectable
@interface HeartView : UIView

@property (nonatomic) UIColor* lineColor;

@end
