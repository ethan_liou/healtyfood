//
//  CustomTabBarController.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/28.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "CustomTabBarController.h"

@interface CustomTabBarController ()<UITabBarControllerDelegate>

@end

@implementation CustomTabBarController

- (void)setSelectedIndex:(NSUInteger)selectedIndex{
    ((UIButton*)[self.view viewWithTag:5566]).selected = selectedIndex == 2;
    [super setSelectedIndex:selectedIndex];
}

- (void)clickButton{
    
    self.selectedIndex = 2;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = 5566;
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    [button setImage:[UIImage imageNamed:@"heart_g.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"heart_b.png"] forState:UIControlStateSelected];
    [button setImage:[UIImage imageNamed:@"heart_b.png"] forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0.0, 0.0, self.tabBar.frame.size.width / 5, self.tabBar.frame.size.height + 15);
    
    [button addTarget:self action:@selector(clickButton) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor whiteColor];
    button.layer.borderWidth = 1.0f;
    button.layer.cornerRadius = 6.0f;
    button.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    button.center = self.tabBar.center;
    [self.view addSubview:button];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    ((UIButton*)[self.view viewWithTag:5566]).selected = NO;
}

@end
