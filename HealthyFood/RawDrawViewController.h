//
//  DrawViewController.h
//  SimpleHR
//
//  Created by Ethan on 2015/2/12.
//  Copyright (c) 2015年 Tutorial. All rights reserved.
//

#import <GLKit/GLKit.h>
#import <UIKit/UIKit.h>

@interface RawDrawViewController : GLKViewController<GLKViewControllerDelegate>{
    GLKBaseEffect * effect;
    GLfloat * drawBuffer;
    float * tempBuffer;
}

-(BOOL)insertData:(float)data;

@end
