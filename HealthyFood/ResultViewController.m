//
//  ResultViewController.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/27.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "ResultViewController.h"
#import "ResultDrawView.h"
#import "Measure.h"

@interface ResultViewController ()

@property (nonatomic, weak) IBOutlet ResultDrawView * rdv;
@property (nonatomic, weak) IBOutlet UILabel * bpm;
@property (nonatomic, weak) IBOutlet UILabel * status;

@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    int diff = _afterMeasure.bps - _beforeMeasure.bps;
    NSString * direction = diff > 0 ? @"上升" : @"下降";
    _bpm.text = [NSString stringWithFormat:@"%@%d次", direction, abs(diff) ];
    
    if(_afterMeasure.bps - _beforeMeasure.bps > 15){
        _rdv.color = [UIColor redColor];
        _status.text = @"敏感";
    } else if(_afterMeasure.bps - _beforeMeasure.bps > 5){
        _rdv.color = [UIColor orangeColor];
        _status.text = @"正常";
    } else {
        _rdv.color = [UIColor greenColor];
        _status.text = @"輕量";
    }
    if(diff > 25)diff = 25;
    if(diff <= 0)diff = 1;
    _rdv.progress = 1.0 * diff / 25;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
