//
//  QuartzDrawView.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/22.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "QuartzDrawView.h"
#import <Accelerate/Accelerate.h>

#define BUF_SIZE 64
#define WINDOW 30
#define LINE_WIDTH 3.

@interface QuartzDrawView()

@property (nonatomic) Float32 * tempBuffer;
@property (nonatomic) Float32 m;
@property (nonatomic) Float32 M;

@end

@implementation QuartzDrawView

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.tempBuffer = calloc(BUF_SIZE, sizeof(Float32));
        self.lineColor = [UIColor blackColor];
    }
    return self;
}

-(void)dealloc{
    if(_tempBuffer) free(_tempBuffer);
}

-(CGFloat)isMinPeak{
    float minVal;
    unsigned long minValIdx;
    vDSP_minvi(&_tempBuffer[BUF_SIZE - WINDOW], 1, &minVal, &minValIdx, WINDOW);
    return minValIdx == WINDOW / 2 ? minVal : 99999;
}

-(CGFloat)isMaxPeak{
    float maxVal;
    unsigned long maxValIdx;
    vDSP_maxvi(&_tempBuffer[BUF_SIZE - WINDOW], 1, &maxVal, &maxValIdx, WINDOW);
    return maxValIdx == WINDOW / 2 ? maxVal : 99999;
}

-(NSData*)getBuffer{
    return [NSData dataWithBytes:_tempBuffer length:BUF_SIZE * sizeof(Float32)];
}

-(void)setBuffer:(NSData*)d{
    memcpy(_tempBuffer, d.bytes, d.length);

    // update frame
    vDSP_maxv(_tempBuffer, 1, &_M, BUF_SIZE);
    vDSP_minv(_tempBuffer, 1, &_m, BUF_SIZE);

    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
    });
}

-(void)insertData:(float)data{
    memmove(_tempBuffer, _tempBuffer+1, (BUF_SIZE-1)*sizeof(float));
    _tempBuffer[BUF_SIZE-1] = data;

    // update frame
    vDSP_maxv(_tempBuffer, 1, &_M, BUF_SIZE);
    vDSP_minv(_tempBuffer, 1, &_m, BUF_SIZE);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
    });
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    if(_M != 0 && _m != 0){
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetShouldAntialias(context, YES);
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        
        CGFloat xDiff = rect.size.width / BUF_SIZE;
        for(int i = 0 ; i < BUF_SIZE ; i++){
            CGPoint pt = CGPointMake(xDiff * i, LINE_WIDTH * 2 + (rect.size.height - 2 * LINE_WIDTH * 2) * (_tempBuffer[i] - _m) / (_M - _m) );
            if(i == 0){
                [path moveToPoint:pt];
            } else {
                [path addLineToPoint:pt];
            }
        }
        path.lineJoinStyle = kCGLineJoinRound;
        path.lineCapStyle = kCGLineCapRound;
        path.lineWidth = LINE_WIDTH;
        [_lineColor setStroke];
        [path stroke];        
    }
}

@end
