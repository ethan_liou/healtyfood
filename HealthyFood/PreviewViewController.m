//
//  PreviewViewController.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/15.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "PreviewViewController.h"
#import <Accelerate/Accelerate.h>

@interface PreviewViewController (){
    float * buffer;
}

@property (nonatomic, strong) AVCaptureSession * session;
@property (nonatomic, strong) IBOutlet UIImageView * preview;

@end

@implementation PreviewViewController

// Create and configure a capture session and start it running
- (void)setupCaptureSession
{
    NSError *error = nil;
    
    // Create the session
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    
    // Configure the session to produce lower resolution video frames, if your
    // processing algorithm can cope. We'll specify medium quality for the
    // chosen device.
    session.sessionPreset = AVCaptureSessionPresetLow;
    
    // Find a suitable AVCaptureDevice
    AVCaptureDevice *device = [AVCaptureDevice
                               defaultDeviceWithMediaType:AVMediaTypeVideo];
    // Create a device input with the device and add it to the session.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device
                                                                        error:&error];
    if (!input) {
        // Handling the error appropriately.
    }
    [session addInput:input];
    
    // Create a VideoDataOutput and add it to the session
    
    AVCaptureVideoDataOutput *output = [AVCaptureVideoDataOutput new];
    output.videoSettings =
    [NSDictionary dictionaryWithObject:
     [NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                forKey:(id)kCVPixelBufferPixelFormatTypeKey];

    [output setAlwaysDiscardsLateVideoFrames:YES];
    
    AVCaptureConnection *conn = [output connectionWithMediaType:AVMediaTypeVideo];
    [conn setVideoOrientation:AVCaptureVideoOrientationPortrait];
    
    // Configure your output.
    dispatch_queue_t queue = dispatch_queue_create("VideoDataOutputQueue", DISPATCH_QUEUE_SERIAL);
    [output setSampleBufferDelegate:self queue:queue];
    
    if([session canAddOutput:output]){
        [session addOutput:output];
    }
    
    // Assign session to an ivar.
    _session = session;
}

// Delegate routine that is called when a sample buffer was written
- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection
{
    // Create a UIImage from the sample buffer data
    CGFloat power = 0;
    UIImage *image = [self imageFromSampleBuffer:sampleBuffer withAveragePower:&power];
    if(_delegate)
        [_delegate dataUpdate:power];
    dispatch_async(dispatch_get_main_queue(), ^{
        _preview.image = image;
    });
}

// Create a UIImage from sample buffer data
- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer withAveragePower:(CGFloat*)power{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    if(buffer == NULL){
        buffer = calloc(4 * width * height, sizeof(float));
    }
    
    vDSP_vflt8(baseAddress, 1, buffer, 1, 4 * width * height);
        
    float R = 0;
    vDSP_sve(buffer, 4, &R, width * height);
    float G = 0;
    vDSP_sve(buffer+1, 4, &G, width * height);
    float B = 0;
    vDSP_sve(buffer+2, 4, &B, width * height);
    
    *power =
    0.299 * (R / width / height) +
    0.587 * (G / width / height) +
    0.114 * (B / width / height);
    
    *power /= 128;
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupCaptureSession];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)start{
    AVCaptureDevice * device = ((AVCaptureDeviceInput*)_session.inputs.lastObject).device;
    if([device hasTorch]){
        if([device lockForConfiguration:nil]){
            [device setActiveVideoMinFrameDuration:CMTimeMake(1, 30)];
            [device setActiveVideoMaxFrameDuration:CMTimeMake(1, 30)];
            [device setTorchMode:AVCaptureTorchModeOn];
            [device unlockForConfiguration];
        }
    }
    [_session startRunning];
    
}

-(void)stop{
    NSLog(@"Stop");
    AVCaptureDevice * device = ((AVCaptureDeviceInput*)_session.inputs.lastObject).device;
    if([device hasTorch]){
        if([device lockForConfiguration:nil]){
            [device setTorchMode:AVCaptureTorchModeOff];
            [device unlockForConfiguration];
        }
    }
    [_session stopRunning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
