//
//  RestaurantViewController.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/19.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "RestaurantViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "MyLocController.h"

@interface RestaurantViewController ()<FBLoginViewDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) NSMutableArray * data;
@property (nonatomic, strong) NSArray * filteredData;
@property (nonatomic, weak) IBOutlet FBLoginView * fbView;
@property (nonatomic, weak) IBOutlet UITableView * tblView;
@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, strong) NSString * currentWord;

@end

@implementation RestaurantViewController

-(IBAction)dismiss{
    if(_delegate){
        [_delegate backWithNothing];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _data = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getRestaurant{
    NSString * param = @"";
    if(_currentWord != nil && _currentWord.length > 0){
        param = [NSString stringWithFormat:@"q=%@&",_currentWord];
    }
    NSString * req = [[NSString stringWithFormat:@"search?%@type=place&distance=1000&center=%f,%f",
                      param,
                      _currentLocation.coordinate.latitude,
                      _currentLocation.coordinate.longitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSSet * targetCategory = [[NSSet alloc] initWithObjects:
                              @"165679780146824",
                              @"273819889375819",
                              @"203462172997785",
                              @"199146280116687",
                              @"203743122984241",
                              @"144362285623268",
                              @"128673187201735",
                              @"2500",nil];
    [FBRequestConnection startWithGraphPath:req
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  [_data removeAllObjects];
                                  NSArray * datas = [result objectForKey:@"data"];
                                  for (NSDictionary * obj in datas) {
                                      BOOL food = NO;
                                      for (NSDictionary * d in [obj objectForKey:@"category_list"]) {
                                          if ([targetCategory containsObject:[d objectForKey:@"id"]]  ) {
                                              food = YES;
                                              break;
                                          }
                                      }
                                      if(food){
                                          NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
                                          [dict setObject:[obj objectForKey:@"name"] forKey:@"name"];
                                          NSDictionary * locObj = [obj objectForKey:@"location"];
                                          [dict setObject:locObj forKey:@"location"];
                                          CLLocation * loc = [[CLLocation alloc] initWithLatitude:((NSString*)[locObj objectForKey:@"latitude"]).doubleValue longitude:((NSString*)[locObj objectForKey:@"longitude"]).doubleValue];
                                          [dict setObject:[NSNumber numberWithDouble:[loc distanceFromLocation:_currentLocation]] forKey:@"distance"];
                                          [dict setObject:loc forKey:@"location"];
                                          [dict setObject:[obj objectForKey:@"id"] forKey:@"id"];
                                          [_data addObject:dict];
                                      }
                                  }
                                  NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
                                  [_data sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
                                  [_tblView reloadData];
                              } else {
                                  
                              }
                          }];
}

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView{
    _fbView.hidden = YES;
    _tblView.hidden = NO;
    self.searchDisplayController.searchBar.hidden = NO;
    if (_currentLocation == nil) {
        [[MyLocController sharedInstance] getLocationWithSuc:^(CLLocation* loc){
            _currentLocation = loc;
            [self getRestaurant];
        } WithErr:^(NSString* errMsg){
            
        }];
    } else {
        [self getRestaurant];
    }
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView{
    _fbView.hidden = NO;
    _tblView.hidden = YES;
    self.searchDisplayController.searchBar.hidden = YES;
}

- (void)loginView:(FBLoginView *)loginView
      handleError:(NSError *)error{
    NSLog(@"Error %@",error);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary * dict = [_data objectAtIndex:indexPath.row];
    cell.textLabel.text = [dict objectForKey:@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.0lfm", ((NSNumber*)[dict objectForKey:@"distance"]).doubleValue];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_delegate){
        [_delegate backWithSuccess:[_data objectAtIndex:indexPath.row]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    _currentWord = searchText;
    [_timer invalidate];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.2 target:self selector:@selector(getRestaurant) userInfo:nil repeats:NO];
}

@end
