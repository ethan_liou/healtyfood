//
//  DrawViewController.m
//  SimpleHR
//
//  Created by Ethan on 2015/2/12.
//  Copyright (c) 2015年 Tutorial. All rights reserved.
//

#import "RawDrawViewController.h"
#import <Accelerate/Accelerate.h>

#define BUF_SIZE 64
#define WINDOW 30

@implementation RawDrawViewController

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        effect = [[GLKBaseEffect alloc] init];
        self.delegate = self;
        
        drawBuffer = new GLfloat[2*BUF_SIZE];
        tempBuffer = new float[BUF_SIZE];
        
        // Let's color the line
        effect.useConstantColor = GL_TRUE;
        
        // Make the line a cyan color
        effect.constantColor = GLKVector4Make(
                                              0.6f, // Red
                                              0.6f, // Green
                                              0.6f, // Blue
                                              1.0f);// Alpha
        
    }
    return self;
}

-(void)dealloc{
    delete drawBuffer; drawBuffer = NULL;
    delete tempBuffer; tempBuffer = NULL;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    GLKView * view = (GLKView *)self.view;
    view.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    view.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // Prepare the effect for rendering
    [effect prepareToDraw];
    
    // Create an handle for a buffer object array
    GLuint bufferObjectNameArray;
    
    // Have OpenGL generate a buffer name and store it in the buffer object array
    glGenBuffers(1, &bufferObjectNameArray);
    
    // Bind the buffer object array to the GL_ARRAY_BUFFER target buffer
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjectNameArray);
    // Send the line data over to the target buffer in GPU RAM
    glBufferData(
                 GL_ARRAY_BUFFER,   // the target buffer
                 BUF_SIZE * 2 * sizeof(GLfloat),       // the number of bytes to put into the buffer
                 drawBuffer,           // a pointer to the data being copied
                 GL_STATIC_DRAW);   // the usage pattern of the data
    
    // Enable vertex data to be fed down the graphics pipeline to be drawn
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    
    // Specify how the GPU looks up the data
    glVertexAttribPointer(
                          GLKVertexAttribPosition, // the currently bound buffer holds the data
                          2,                       // number of coordinates per vertex
                          GL_FLOAT,                // the data type of each component
                          GL_FALSE,                // can the data be scaled
                          2*4,                     // how many bytes per vertex (2 floats per vertex)
                          NULL);                   // offset to the first coordinate, in this case 0
    
    glLineWidth(8.0f);
    
    glDrawArrays(GL_LINE_STRIP, 0, BUF_SIZE); // render
    
    glDeleteBuffers(1, &bufferObjectNameArray);
    
    self.paused = NO;
}

-(BOOL)insertData:(float)data{
    memmove(tempBuffer, tempBuffer+1, (BUF_SIZE-1)*sizeof(float));
    tempBuffer[BUF_SIZE-1] = data;
    float minVal;
    unsigned long minValIdx;
    vDSP_minvi(&tempBuffer[BUF_SIZE - WINDOW], 1, &minVal, &minValIdx, WINDOW);
    return minValIdx == WINDOW / 2;
}

- (void)glkViewControllerUpdate:(GLKViewController *)controller{
    Float32 M=1,m=0;
    vDSP_maxv(tempBuffer, 1, &M, BUF_SIZE-1);
    vDSP_minv(tempBuffer, 1, &m, BUF_SIZE-1);

    for(int i = 0 ; i < BUF_SIZE ; i ++){
        drawBuffer[2 * i] = -1. + 2. * i / BUF_SIZE;
        drawBuffer[2 * i + 1] = -1. + 2. * (tempBuffer[i] - m) / (M-m);
    }
}

@end
