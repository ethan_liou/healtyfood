//
//  ResultLocationSelectorViewController.m
//  HealthyFood
//
//  Created by Ethan on 2015/4/2.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "ResultLocationSelectorViewController.h"
#import "Location.h"
#import "MyLocController.h"

@interface ResultLocationSelectorViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) NSArray * data;
@property (nonatomic, strong) CLLocation * currentLoc;

@end

@implementation ResultLocationSelectorViewController

- (IBAction)dismiss{
    if(_delegate){
        [_delegate backWithNothing];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)reloadView{
    _data = [Location getAllLocations];
    for (Location * loc in _data) {
        CLLocation * l = [[CLLocation alloc] initWithLatitude:loc.lat longitude:loc.lng];
        loc.disWithSelf = [l distanceFromLocation:_currentLoc];
    }
    _data = [_data sortedArrayUsingComparator:^NSComparisonResult(Location * obj1, Location * obj2) {
        return obj1.disWithSelf - obj2.disWithSelf;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[MyLocController sharedInstance] getLocationWithSuc:^(CLLocation* loc){
        _currentLoc = loc;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadView];
        });
    } WithErr:^(NSString* errStr){
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell2";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    Location * loc = [_data objectAtIndex:indexPath.row];
    cell.textLabel.text = loc.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.0lfm",loc.disWithSelf];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_delegate){
        [_delegate backWithSuccess:[_data objectAtIndex:indexPath.row]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
//    _currentWord = searchText;
//    [_timer invalidate];
//    _timer = [NSTimer scheduledTimerWithTimeInterval:1.2 target:self selector:@selector(getRestaurant) userInfo:nil repeats:NO];
}

@end
