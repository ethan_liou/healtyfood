//
//  Location.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/28.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMDatabase;

@interface Location : NSObject<NSCopying>

@property (nonatomic) long long objId;
@property (nonatomic, strong) NSString * name;
@property (nonatomic) double lat;
@property (nonatomic) double lng;
@property (nonatomic) double disWithSelf;

-(void)saveWithDB:(FMDatabase*)fdb;
+(Location*)getLocationByObjId:(long long)objId WithDB:(FMDatabase*)fdb;
+(NSArray*)getAllLocations;

@end
