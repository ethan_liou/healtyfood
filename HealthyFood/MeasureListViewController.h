//
//  MeasureListViewController.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/28.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"

@interface MeasureListViewController : UIViewController

@property (nonatomic, weak) Location * location;

@end
