//
//  Measure.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/22.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "Measure.h"
#import "DBUtils.h"

@implementation Measure

-(id)copyWithZone:(NSZone *)zone{
    Measure * newInstance = [[self.class allocWithZone:zone] init];
    newInstance.timing = _timing;
    newInstance.measureTime = [_measureTime copyWithZone:zone];
    newInstance.bps = _bps;
    newInstance.user = [_user copyWithZone:zone];
    newInstance.rawData = [_rawData copyWithZone:zone];
    newInstance.location = [_location copyWithZone:zone];
    newInstance.reference = _reference;
    return newInstance;
}

-(id)init{
    self = [super init];
    if(self){
        self.user = [Friend getSelf];
        self.reference = -1;
    }
    return self;
}

-(NSString*)description{
    return [NSString stringWithFormat:@"%@<%@>",_user, _measureTime];
}

-(NSDictionary*)dict{
    NSDictionary * d;
    d = @{@"timing":[NSNumber numberWithInteger:_timing],
          @"measureTime":_measureTime,
          @"bps":[NSNumber numberWithInteger:_bps],
          @"rawData":_rawData,
          @"location":_location == nil ? [NSNull null] : [NSNumber numberWithLongLong:_location.objId],
          @"user":[NSNumber numberWithInteger:_user.friendId],
          @"reference":[NSNumber numberWithInteger:_reference]};
    return d;
}

+(Measure*)getMeasureFromRs:(FMResultSet*)rs WithDB:(FMDatabase*)db{
    Measure * measure = [[Measure alloc] init];
    measure.objId = [rs intForColumn:@"measureId"];
    measure.timing = [rs intForColumn:@"timing"];
    measure.measureTime = [rs dateForColumn:@"measureTime"];
    measure.bps = [rs intForColumn:@"bps"];
    measure.rawData = [rs dataForColumn:@"rawData"];
    if(![rs columnIsNull:@"location"]){
        measure.location = [Location getLocationByObjId:[rs longLongIntForColumn:@"location"] WithDB:db];
    }
    measure.reference = [rs intForColumn:@"reference"];
    measure.user = [Friend getFriendByObjId:[rs intForColumn:@"user"] WithDB:db];
    return measure;
}

-(void)save{
    FMDatabaseQueue * db = [DBUtils sharedInstance];
    [db inDatabase:^(FMDatabase* db){
        if(_user != nil){
            [_user saveWithDB:db];
        }
        if(_location != nil){
            [_location saveWithDB:db];
        }
        [db executeUpdate:@"INSERT INTO measure (timing, measureTime, bps, rawData, user, location, reference) VALUES (:timing, :measureTime, :bps, :rawData, :user, :location, :reference)" withParameterDictionary: [self dict] ];
    }];
}

+(Measure*)getMeasureById:(int)objId{
    FMDatabaseQueue * db = [DBUtils sharedInstance];
    __block Measure * measure = nil;
    [db inDatabase:^(FMDatabase* db){
        FMResultSet * rs = [db executeQueryWithFormat:@"SELECT * FROM measure WHERE measureId = %d", objId];
        while(rs.next){
            measure = [self getMeasureFromRs:rs WithDB:db];
        }
    }];
    return measure;
}

+(NSMutableArray*)getTodayMeasure{
    FMDatabaseQueue * db = [DBUtils sharedInstance];
    __block NSMutableArray * measures = [[NSMutableArray alloc] init];
    [db inDatabase:^(FMDatabase* db){
        FMResultSet * rs = [db executeQuery:@"SELECT * FROM measure WHERE date(measureTime) > date('now','-1 days')"];
        while(rs.next){
            [measures addObject:[self getMeasureFromRs:rs WithDB:db]];
        }
    }];
    return measures;
}

+(Measure*)getLastMeasure{
    FMDatabaseQueue * db = [DBUtils sharedInstance];
    __block Measure * measure = nil;
    [db inDatabase:^(FMDatabase* db){
        FMResultSet * rs = [db executeQuery:@"SELECT * from measure ORDER BY measureId DESC LIMIT 1"];
        while(rs.next){
            measure = [self getMeasureFromRs:rs WithDB:db];
        }
    }];
    return measure;
}

+(NSArray*)getMeasuresInLocID:(long long)locId{
    FMDatabaseQueue * db = [DBUtils sharedInstance];
    __block NSMutableArray * measures = [[NSMutableArray alloc] init];
    [db inDatabase:^(FMDatabase* db){
        FMResultSet * rs = [db executeQueryWithFormat:@"SELECT * FROM measure WHERE location=%lld ORDER BY measureId DESC",locId];
        while(rs.next){
            Measure * measure = [self getMeasureFromRs:rs WithDB:db];
;
            [measures addObject:measure];
        }
    }];
    return measures;
}

@end
