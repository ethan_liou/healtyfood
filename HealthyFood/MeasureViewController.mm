//
//  MeasureViewController.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/16.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "MeasureViewController.h"
#import <Accelerate/Accelerate.h>
#import "RoundedProgressView.h"
#import "PreviewViewController.h"
#import "QuartzDrawView.h"
#import <vector>

@interface MeasureViewController ()<PreviewDataUpdate>{
    std::vector<float> xVec;
    std::vector<float> yVec;
}

@property (nonatomic, weak) IBOutlet RoundedProgressView * progressView;
@property (nonatomic, weak) IBOutlet UILabel * heartBeatLbl;
@property (nonatomic, weak) IBOutlet QuartzDrawView * drawView;
@property (nonatomic, weak) PreviewViewController * previewController;
@property (nonatomic) float lastTime;
@property (nonatomic) BOOL run;
@property (nonatomic) float lastMaxX;
@property (nonatomic) float lastMaxY;
@property (nonatomic) BOOL needRecover;

@end

@implementation MeasureViewController

#define TOTAL_STEP 2

-(void)handleNoti:(NSNotification *)noti{
    if(noti.name == UIApplicationWillResignActiveNotification){
        [self stop];
    } else {
        if(_needRecover){
            [self start];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    for(UIViewController * controller in self.childViewControllers){
        if([controller isKindOfClass:PreviewViewController.class]){
            _previewController = (PreviewViewController*)controller;
        }
    }
    _previewController.delegate = self;
    _run = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNoti:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNoti:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [self start];
}

- (void)start{
    if(!_run){
        xVec.clear();
        yVec.clear();
        _lastTime = 0;
        _lastMaxX = -1;
        _run = YES;
        _needRecover = YES;
        _progressView.progress = 0;
        [_previewController start];
    }
}

- (void)stop{
    if (_run) {
        _run = NO;
        [_previewController stop];
    }
}

- (void)measureFinish{
    _needRecover = NO;
    [self stop];
}

- (void)viewDidDisappear:(BOOL)animated{
    [self stop];
}

float calStdev(std::vector<float>& vec, float mean){
    if(vec.size() == 1){
        return 0;
    } else{
        float total = 0;
        for(int i = 0 ; i < vec.size() ; i ++){
            total += (( vec[i] - mean ) * ( vec[i] - mean ));
        }
        return sqrt(total / (vec.size() - 1));
    }
}

-(void)dataUpdate:(CGFloat)data{
    [_drawView insertData:data];
    CGFloat m = [_drawView isMinPeak],M = [_drawView isMaxPeak];
    if(M != 99999){// is max peak
        _lastMaxX = CACurrentMediaTime();
        _lastMaxY = M;
    }
    CGPoint pt = CGPointZero;
    if(m != 99999 && _lastMaxX > 0){
        pt.x = (_lastMaxX + CACurrentMediaTime()) / 2;
        pt.y = (_lastMaxY + m) / 2;
    }
    if (pt.x != 0 && pt.y != 0) {
        __block float heartBeatShow = 0;
        
        if(_lastTime != 0){
            if (xVec.size() == TOTAL_STEP) {
                xVec.erase(xVec.begin());
                yVec.erase(yVec.begin());
            }
            xVec.push_back(pt.x - _lastTime);
            yVec.push_back(pt.y);
            heartBeatShow = 60. / *xVec.rbegin();
        }
        _lastTime = pt.x;
        
        // calculate heartbeat
        __block float heartBeat = 0;
        __weak MeasureViewController * wSelf = self;
        
        float meanX = 0;
        vDSP_meanv(xVec.data(), 1, &meanX, xVec.size());
        float stdevX = calStdev(xVec, meanX);

        float meanY = 0;
        vDSP_meanv(yVec.data(), 1, &meanY, yVec.size());
        float stdevY = calStdev(yVec, meanY);

        heartBeat = 60. / meanX;

        if((stdevY / meanY) > 0.05 || (stdevX / meanX) > 0.05 || heartBeat < 40 || heartBeat > 200){
            xVec.erase(xVec.begin());
            yVec.erase(yVec.begin());
        } else{
            [_progressView incProgress:1.0 / (TOTAL_STEP + 1)];
        }
        
        if(xVec.size() == TOTAL_STEP){
            [self measureFinish];
            if(_delegate){
                std::vector<float> tmpVec(xVec);
                tmpVec.insert(tmpVec.end(), yVec.begin(), yVec.end());
                [_delegate measureDone:(int)heartBeat WithData:[_drawView getBuffer]];
            }
            heartBeatShow = heartBeat;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(heartBeatShow != 0){
                wSelf.heartBeatLbl.text = [NSString stringWithFormat:@"%.0f",heartBeatShow];
            }
//            AudioServicesPlaySystemSound (1070);
        });
    }
}

@end
