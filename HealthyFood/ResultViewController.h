//
//  ResultViewController.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/27.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Measure;

@interface ResultViewController : UIViewController

@property (nonatomic, strong) Measure * beforeMeasure;
@property (nonatomic, strong) Measure * afterMeasure;

@end
