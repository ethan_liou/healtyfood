//
//  Friend.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/28.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMDatabase;
@interface Friend : NSObject<NSCopying>

@property (nonatomic) int friendId;
@property (nonatomic, strong) NSString * friendName;
@property (nonatomic) long long fbId;

-(void)saveWithDB:(FMDatabase*)fdb;
+(Friend*)getFriendByObjId:(int)objId WithDB:(FMDatabase*)db;
+(Friend*)getSelf;

@end
