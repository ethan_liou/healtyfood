//
//  MeasureTableViewCell.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/28.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Measure.h"

@interface MeasureTableViewCell : UITableViewCell

@property (nonatomic, weak) Measure * measure;
@property (nonatomic, weak) IBOutlet UILabel * detail;

@end
