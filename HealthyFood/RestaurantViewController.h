//
//  RestaurantViewController.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/19.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallbackProtocol.h"
#import <CoreLocation/CoreLocation.h>

@interface RestaurantViewController : UIViewController

@property (nonatomic, weak) id<CallbackProtocol> delegate;
@property (nonatomic, strong) CLLocation * currentLocation;

@end
