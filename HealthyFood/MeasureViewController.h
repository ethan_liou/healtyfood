//
//  MeasureViewController.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/16.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MeasureDelegate <NSObject>

-(void)measureDone:(int)heartBeat WithData:(NSData*)x;

@end

@interface MeasureViewController : UIViewController

@property (nonatomic, weak) id<MeasureDelegate> delegate;
- (void)start;
- (void)stop;

@end
