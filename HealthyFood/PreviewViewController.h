//
//  PreviewViewController.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/15.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol PreviewDataUpdate

-(void)dataUpdate:(CGFloat)data;

@end

@interface PreviewViewController : UIViewController<AVCaptureVideoDataOutputSampleBufferDelegate>

@property id<PreviewDataUpdate> delegate;
-(void)start;
-(void)stop;

@end
