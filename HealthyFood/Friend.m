//
//  Friend.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/28.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "Friend.h"
#import "DBUtils.h"

@implementation Friend

static Friend * myself = nil;

-(id)copyWithZone:(NSZone *)zone{
    Friend * newInstance = [[self.class allocWithZone:zone] init];
    newInstance.friendId = _friendId;
    newInstance.friendName = [_friendName copyWithZone:zone];
    newInstance.fbId = _fbId;
    return newInstance;
}

-(NSString*)friendName{
    return _friendId == 0 ? @"自己" : _friendName;
}

-(id)init{
    self = [super init];
    if(self){
        self.friendId = -1;
    }
    return self;
}

-(NSDictionary*)dict{
    return  @{@"friendId":[NSNumber numberWithLongLong:_friendId],
              @"friendName":_friendName,
              @"fbId":[NSNumber numberWithLongLong:_fbId]};
}

+(Friend*)getSelf{
    if(myself == nil){
        myself = [Friend getFriendByObjId:0 WithDB:nil];
    }
    return myself;
}

-(void)saveWithDB:(FMDatabase*)fdb{
    __weak Friend * wSelf = self;
    void (^dbAction)(FMDatabase *)=^(FMDatabase* db){
        if(wSelf.friendId < 0){
            [db executeUpdate:@"INSERT INTO friend (friendName, fbId)  VALUES (:friendName, :fbId)" withParameterDictionary: [self dict] ];
            wSelf.friendId = (int)[db lastInsertRowId];
        } else {
            [db executeUpdate:@"UPDATE friend SET friendName = :friendName, fbId = :fbId WHERE friendId=:friendId" withParameterDictionary: [self dict] ];
        }
    };
    if(fdb == nil){
        FMDatabaseQueue * db = [DBUtils sharedInstance];
        [db inDatabase:dbAction];
    } else {
        dbAction(fdb);
    }
}

+(Friend*)getFriendByObjId:(int)objId WithDB:(FMDatabase*)fdb{
    __block Friend * friend = nil;
    void (^dbAction)(FMDatabase *)=^(FMDatabase* db){
        FMResultSet * rs = [db executeQueryWithFormat:@"SELECT * from friend WHERE friendId = %d LIMIT 1", objId];
        while(rs.next){
            friend = [[Friend alloc] init];
            friend.friendId = objId;
            friend.friendName = [rs stringForColumn:@"friendName"];
            friend.fbId = [rs longLongIntForColumn:@"fbId"];
        }
    };
    if(fdb == nil){
        FMDatabaseQueue * db = [DBUtils sharedInstance];
        [db inDatabase:dbAction];
    } else{
        dbAction(fdb);
    }
    return friend;
}

@end
