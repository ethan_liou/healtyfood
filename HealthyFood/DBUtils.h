//
//  DBUtils.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/22.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDB.h>

@interface DBUtils : NSObject

+(FMDatabaseQueue*)sharedInstance;

@end
