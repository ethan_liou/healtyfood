//
//  MeasureListViewController.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/28.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "MeasureListViewController.h"
#import "Measure.h"
#import "MeasureTableViewCell.h"
#import "HeartBeatViewController.h"
#import "ResultLocationSelectorViewController.h"
#import "ResultViewController.h"

@interface MeasureListViewController ()<UITableViewDataSource, UITableViewDelegate, CallbackProtocol>

@property (nonatomic, weak) IBOutlet UITableView * measureTV;
@property (nonatomic, strong) NSMutableArray * measures;
@property (nonatomic, strong) Measure * lastMeasure;
@property (nonatomic, strong) Location * currLocation;

@end

@implementation MeasureListViewController

- (void)reloadView{
    self.title = _currLocation.name;
    _measures = [[Measure getMeasuresInLocID:_currLocation.objId] mutableCopy];
    [_measureTV reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    if(_currLocation == nil){
        _lastMeasure = [Measure getLastMeasure];
        if(_lastMeasure != nil){
            _currLocation = _lastMeasure.location;
        }
    }
    [self reloadView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _measures.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"MeasureCell";
    
    MeasureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[MeasureTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.measure = [_measures objectAtIndex:indexPath.row];
    if(cell.measure.timing == 0){
        // before
        cell.detail.text = @"測量飯後";
    } else {
        cell.detail.text = @"觀看報告";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Measure * measure = (Measure*)[_measures objectAtIndex:indexPath.row];
    if(measure.timing == 0){
        // before
        HeartBeatViewController * hbvc = [self.tabBarController.viewControllers objectAtIndex:2];
        hbvc.measure = measure.copy;
        hbvc.measure.timing = 1;
        hbvc.measure.reference = measure.objId;
        self.tabBarController.selectedIndex = 2;
    } else {
        [self performSegueWithIdentifier:@"ToResult" sender:measure];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_measures removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:ResultLocationSelectorViewController.class]){
        ResultLocationSelectorViewController * vc = [segue destinationViewController];
        vc.delegate = self;
    } else if([segue.destinationViewController isKindOfClass:ResultViewController.class]){
        Measure * measure = sender;
        ResultViewController * vc = segue.destinationViewController;
        vc.afterMeasure = measure;
        vc.beforeMeasure = [Measure getMeasureById:measure.reference];
    }
}

#pragma callback

-(void)backWithSuccess:(id)data{
    _currLocation = data;
    [self reloadView];
}

-(void)backWithNothing{
    
}

@end
