//
//  ResultLocationSelectorViewController.h
//  HealthyFood
//
//  Created by Ethan on 2015/4/2.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallbackProtocol.h"
#import <CoreLocation/CoreLocation.h>

@interface ResultLocationSelectorViewController : UIViewController

@property (nonatomic, weak) id<CallbackProtocol> delegate;

@end
