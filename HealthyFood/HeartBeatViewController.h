//
//  HeartBeatViewController.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/19.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Measure.h"

@interface HeartBeatViewController : UIViewController

@property (nonatomic, strong) Measure * measure;

@end
