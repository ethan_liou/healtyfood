//
//  RoundedProgressView.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/16.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface RoundedProgressView : UIView

@property (nonatomic, strong) UIColor * color;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat progress;
@property (nonatomic) CGFloat startAngle;
@property (nonatomic) CGFloat endAngle;
@property (nonatomic) BOOL clockwise;

-(void)incProgress:(CGFloat) progress;

@end
