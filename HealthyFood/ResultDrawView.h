//
//  ResultDrawView.h
//  HealthyFood
//
//  Created by Ethan on 2015/4/6.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultDrawView : UIView

@property (nonatomic, strong) UIColor * color;
@property (nonatomic) CGFloat progress;

@end
