//
//  DialogCallbackProtocol.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/20.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#ifndef HealthyFood_DialogCallbackProtocol_h
#define HealthyFood_DialogCallbackProtocol_h

@protocol CallbackProtocol <NSObject>

@optional
-(void)backWithSuccess:(id)data;
-(void)backWithNothing;

@end

#endif
