//
//  MyLocController.m
//  HealthyFood
//
//  Created by Ethan on 2015/4/6.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "MyLocController.h"
#import <CoreLocation/CoreLocation.h>

@interface MyLocController()<CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (copy) LocationCallback callback;
@property (copy) LocationErrCallback errCallback;

@end

@implementation MyLocController

#define LAST_MEASURE_TIME @"lastMeasureTime"
#define LAST_MEASURE_LAT @"lastMeasureLat"
#define LAST_MEASURE_LNG @"lastMeasureLng"
#define TIME_THRESHOLD 5 * 60

+(MyLocController*)sharedInstance{
    static MyLocController *sharedInstance = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[MyLocController alloc] init];
    });
    
    return sharedInstance;
}

-(void)getLocationWithSuc:(LocationCallback)callback WithErr:(LocationErrCallback)errCallback{

    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:LAST_MEASURE_TIME] != nil &&
       [defaults objectForKey:LAST_MEASURE_LAT] != nil &&
       [defaults objectForKey:LAST_MEASURE_LNG] != nil){
        NSDate * lastDate = [defaults objectForKey:LAST_MEASURE_TIME];
        if([lastDate timeIntervalSinceNow] * -1 < TIME_THRESHOLD){
            NSLog(@"Cache time!");
            double lat = [defaults doubleForKey:LAST_MEASURE_LAT];
            double lng = [defaults doubleForKey:LAST_MEASURE_LNG];
            callback([[CLLocation alloc] initWithLatitude:lat longitude:lng]);
            return;
        }
    }
    
    _callback = callback;
    _errCallback = errCallback;
    _locationManager = [CLLocationManager new];
    _locationManager.delegate = self;
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    [_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    _currentLocation = [locations objectAtIndex:0];
    [_locationManager stopUpdatingLocation];
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:LAST_MEASURE_TIME];
    [defaults setObject:[NSNumber numberWithDouble:_currentLocation.coordinate.longitude] forKey:LAST_MEASURE_LNG];
    [defaults setObject:[NSNumber numberWithDouble:_currentLocation.coordinate.latitude] forKey:LAST_MEASURE_LAT];
    [defaults synchronize];
    if(_callback != nil){
        _callback(_currentLocation);
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    if(_errCallback != nil){
        _errCallback(error.localizedDescription);
    }
}

@end
