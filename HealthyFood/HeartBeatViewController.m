//
//  HeartBeatViewController.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/19.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "HeartBeatViewController.h"
#import "CallbackProtocol.h"
#import "RestaurantViewController.h"
#import "MeasureViewController.h"
#import "MeasureListViewController.h"
#import <SVProgressHUD.h>
#import "MyLocController.h"

@interface HeartBeatViewController ()<CallbackProtocol, MeasureDelegate, UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UIButton * selectPlaceBtn;
@property (nonatomic, weak) IBOutlet UISegmentedControl * timingSegCtrl;
@property (nonatomic, weak) IBOutlet UISegmentedControl * targetSegCtrl;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * targetRContraint;
@property (nonatomic, weak) IBOutlet UIButton * saveBtn;
@property (nonatomic, weak) MeasureViewController * measureView;
@property (nonatomic, weak) IBOutlet UIView * actionView;
@property (nonatomic, weak) IBOutlet UIView * friendView;
@property (nonatomic) BOOL restart;

@end

@implementation HeartBeatViewController

- (void)updateView{
    if(_measure == nil){
        _timingSegCtrl.selectedSegmentIndex = 0;
        _targetSegCtrl.selectedSegmentIndex = 0;
    } else {
        _timingSegCtrl.selectedSegmentIndex = _measure.timing;
        if(_measure.user.friendId != 0){
            [_targetSegCtrl setTitle:_measure.user.friendName forSegmentAtIndex:1];
            _targetSegCtrl.selectedSegmentIndex = 1;
        } else {
            _targetSegCtrl.selectedSegmentIndex = 0;
        }
        if(_measure.location != nil){
            [_selectPlaceBtn setTitle:_measure.location.name forState:UIControlStateNormal];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    for(UIViewController * controller in self.childViewControllers){
        if([controller isKindOfClass:MeasureViewController.class]){
            _measureView = (MeasureViewController*)controller;
        }
    }
    _measureView.delegate = self;
    _restart = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    if (_restart) {
        _actionView.hidden = YES;
        _friendView.hidden = YES;
        [self updateView];
        if(_measure == nil){
            _measure = [[Measure alloc] init];
        }
        [_measureView start];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [_measureView stop];
    _restart = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:RestaurantViewController.class]){
        CLLocation * loc = (CLLocation *)sender;
        RestaurantViewController * vc = [segue destinationViewController];
        vc.currentLocation = loc;
        vc.delegate = self;        
    } else if([segue.destinationViewController isKindOfClass:MeasureListViewController.class]){
        MeasureListViewController * vc = [segue destinationViewController];
        vc.location = _measure.location;
    }
}

-(void)backWithNothing{
    _restart = NO;
}

-(void)backWithSuccess:(id)data{
    _restart = NO;
    CLLocation * loc = [data objectForKey:@"location"];
    Location * location = [[Location alloc] init];
    location.lng = loc.coordinate.longitude;
    location.lat = loc.coordinate.latitude;
    location.name = [data objectForKey:@"name"];
    location.objId = ((NSString*)[data objectForKey:@"id"]).longLongValue;
    _measure.location = location;
    [self updateView];
}

-(void)measureDone:(int)heartBeat WithData:(NSData*)x{
    _measure.bps = heartBeat;
    _measure.measureTime = [NSDate date];
    _measure.rawData = [NSData dataWithData:x];
    __weak HeartBeatViewController * wSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        wSelf.actionView.hidden = NO;
    });
}

- (IBAction)selectLocation{
    [[MyLocController sharedInstance] getLocationWithSuc:^(CLLocation* loc){
        [self performSegueWithIdentifier:@"SelectRestaurant" sender:loc];
    } WithErr:^(NSString* errStr){
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showErrorWithStatus:errStr maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
}

-(IBAction)retest{
    _actionView.hidden = YES;
    [_measureView start];
}

-(IBAction)save{
    if(_measure.location == nil){
        [SVProgressHUD showErrorWithStatus:@"請選擇地點" maskType:SVProgressHUDMaskTypeGradient];
    } else {
        _measure.timing = (int)_timingSegCtrl.selectedSegmentIndex;
        [_measure save];
        self.tabBarController.selectedIndex = 0;
    }
}

-(IBAction)targetChanged:(UISegmentedControl*)seg{
    if(seg == _targetSegCtrl){
        if(seg.selectedSegmentIndex == 0){
            // self
            _measure.user = [Friend getSelf];
        } else {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"朋友暱稱" message:@"這暱稱只是用來識別" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            if(_measure.user != nil && _measure.user.friendId != 0){
                [alert textFieldAtIndex:0].text = _measure.user.friendName;
            } else {
                [alert textFieldAtIndex:0].text = @"朋友";
            }
            [alert show];
        }
    } else if(seg == _timingSegCtrl){
        _measure.timing = (int)seg.selectedSegmentIndex;
    }
}

//-(IBAction)typeName{
//    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"朋友暱稱" message:@"這暱稱只是用來識別" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
//    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
//    [alert show];
//}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        _measure.user = [[Friend alloc] init];
        _measure.user.friendName = [alertView textFieldAtIndex:0].text;
        [self updateView];
    }
}

@end
