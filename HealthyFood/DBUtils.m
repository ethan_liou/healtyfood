//
//  DBUtils.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/22.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "DBUtils.h"

@interface DBUtils()

@property (nonatomic, strong) FMDatabaseQueue * db;

@end

@implementation DBUtils

+(FMDatabase*)sharedInstance{
    static DBUtils *sharedInstance = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance = [[DBUtils alloc] init];
    });
    
    return sharedInstance.db;
}

-(NSString *)getSqlitePath{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"food.sqlite"];
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    if (!success) {
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"food.sqlite"];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        
        if (!success) {
            NSLog(@"Failed to create writable DB. Error '%@'.", [error localizedDescription]);
        } else {
            NSLog(@"DB copied.");
        }
    }else {
        NSLog(@"DB exists, no need to copy.");
    }
    return dbPath;
}

-(id)init{
    self = [super init];
    if(self){
        NSString * path = [self getSqlitePath];
        _db = [FMDatabaseQueue databaseQueueWithPath:path];
    }
    return self;
}

@end
