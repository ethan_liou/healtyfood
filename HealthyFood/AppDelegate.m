//
//  AppDelegate.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/15.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Measure.h"
#import "Friend.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)jumpToAnalyzeIfNeed{
    NSLog(@"%@",[Measure getTodayMeasure]);
    Measure * lastMeasure = [Measure getLastMeasure];
    UITabBarController* ctrl = (UITabBarController*)self.window.rootViewController;
    if(lastMeasure == nil || [[NSDate date] timeIntervalSinceDate:lastMeasure.measureTime] > 30 * 60 ){
        ctrl.selectedIndex = 2;
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Friend getSelf];
    sleep(1.5);
    [self jumpToAnalyzeIfNeed];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self jumpToAnalyzeIfNeed];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // Call FBAppCall's handleOpenURL:sourceApplication to handle Facebook app responses
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    return wasHandled;
}

@end
