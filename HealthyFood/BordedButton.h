//
//  BordedButton.h
//  HealthyFood
//
//  Created by Ethan on 2015/3/31.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface BordedButton : UIButton

@end
