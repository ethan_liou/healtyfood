//
//  Location.m
//  HealthyFood
//
//  Created by Ethan on 2015/3/28.
//  Copyright (c) 2015年 VinoTechs. All rights reserved.
//

#import "Location.h"
#import "DBUtils.h"

@implementation Location

- (id)copyWithZone:(NSZone *)zone{
    Location * newInstance = [[self.class allocWithZone:zone] init];
    newInstance.objId = _objId;
    newInstance.name = [_name copyWithZone:zone];
    newInstance.lat = _lat;
    newInstance.lng = _lng;
    return newInstance;
}

-(NSDictionary*)dict{
    return  @{@"locationId":[NSNumber numberWithLongLong:_objId],
              @"name":_name,
              @"lat":[NSNumber numberWithDouble:_lat],
              @"lng":[NSNumber numberWithDouble:_lng]};
}

-(void)saveWithDB:(FMDatabase*)fdb{
    void (^dbAction)(FMDatabase *)=^(FMDatabase* db){
        [db executeUpdate:@"INSERT OR IGNORE INTO location VALUES (:locationId, :name, :lat, :lng)" withParameterDictionary: [self dict] ];
    };
    if(fdb == nil){
        FMDatabaseQueue * db = [DBUtils sharedInstance];
        [db inDatabase:dbAction];
    } else {
        dbAction(fdb);
    }
}

+(Location*)getLocationByObjId:(long long)objId WithDB:(FMDatabase*)fdb{
    __block Location * location = nil;
    void (^dbAction)(FMDatabase *)=^(FMDatabase* db){
        FMResultSet * rs = [db executeQueryWithFormat:@"SELECT * from location WHERE locationId = %lld LIMIT 1", objId];
        while(rs.next){
            location = [[Location alloc] init];
            location.objId = objId;
            location.name = [rs stringForColumn:@"name"];
            location.lat = [rs doubleForColumn:@"lat"];
            location.lng = [rs doubleForColumn:@"lng"];
        }
    };
    if(fdb == nil){
        FMDatabaseQueue * db = [DBUtils sharedInstance];
        [db inDatabase:dbAction];
    } else{
        dbAction(fdb);
    }
    return location;
}

+(NSArray*)getAllLocations{
    FMDatabaseQueue * db = [DBUtils sharedInstance];
    NSMutableArray * arr = [[NSMutableArray alloc] init];
    [db inDatabase:^(FMDatabase*db){
        FMResultSet * rs = [db executeQuery:@"SELECT * FROM location"];
        while(rs.next){
            Location * location = [[Location alloc] init];
            location.objId = [rs longLongIntForColumn:@"locationId"];
            location.name = [rs stringForColumn:@"name"];
            location.lat = [rs doubleForColumn:@"lat"];
            location.lng = [rs doubleForColumn:@"lng"];
            [arr addObject:location];
        }
    }];
    return arr;
}

@end
